<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?= $judul; ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="<?= base_url('assets/') ?>/css/style.css" />
</head>

<body>
    <main></main>
    <!-- Header Start -->

    <header id="header">

        <div class="overlay overlay-lg">
            <img src="<?= base_url('assets/') ?>img/shapes/square.png" class="shape square" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/circle.png" class="shape circle" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/half-circle.png" class="shape half-circle1" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/half-circle.png" class="shape half-circle2" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/x.png" class="shape xshape" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/wave.png" class="shape wave wave1" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/wave.png" class="shape wave wave2" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/triangle.png" class="shape triangle" alt="" />
            <img src="<?= base_url('assets/') ?>img/shapes/points1.png" class="points points1" alt="" />
        </div>

        <nav>
            <div class="container">
                <div class="logo">
                    <img src="<?= base_url('assets/') ?>img/logo.png" alt="" />
                </div>

                <div class="links">
                    <ul>
                        <li>
                            <a href="#header">Home</a>
                        </li>
                        <li>
                            <a href="#services">Services</a>
                        </li>
                        <li>
                            <a href="#blog">Blog</a>
                        </li>
                        <li>
                            <a href="#about">About</a>
                        </li>
                        <li>
                            <a href="#contact">Contact</a>
                        </li>
                        <li>
                            <a href="#hireme" class="active">Hire me</a>
                        </li>
                    </ul>
                </div>

                <div class="hamburger-menu">
                    <div class="bar"></div>
                </div>
            </div>
        </nav>

        <div class="header-content">
            <div class="container grid-2">
                <div class="column-1">
                    <h1 class="header-title">Ardhian Sulistyo</h1>
                    <p class="text">
                        Hello, I'm Ardhian Sulistyo, Web Design dan Developer I like to
                        learn and keep learning about IT world
                    </p>
                    <a href="#" class="btn">Download CV</a>
                </div>

                <div class="column-2 image">
                    <img src="<?= base_url('assets/') ?>img/person.png" class="img-element z-index" alt="" />
                </div>
            </div>
        </div>
    </header>

    <!-- Header End -->